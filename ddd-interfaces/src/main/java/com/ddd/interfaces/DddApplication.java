package com.ddd.interfaces;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动
 *
 * @author lizhifu
 * @date 2022/3/8
 */
@SpringBootApplication
public class DddApplication {
    public static void main(String[] args) {
        SpringApplication.run(DddApplication.class, args);
    }
}
