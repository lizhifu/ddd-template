package com.ddd.infrastructure.repository;

import com.ddd.domain.car.model.vo.CarVO;
import com.ddd.domain.car.repository.CarRepository;
import com.ddd.infrastructure.dao.CarDao;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

/**
 * 仓储服务-车
 *
 * @author lizhifu
 * @date 2022/3/9
 */
@Repository
public class CarRepositoryImpl implements CarRepository {
    @Resource
    private CarDao carDao;
    @Override
    public void addCar(CarVO carVO) {

    }

    @Override
    public CarVO queryCar(Long carId) {
        return null;
    }
}
