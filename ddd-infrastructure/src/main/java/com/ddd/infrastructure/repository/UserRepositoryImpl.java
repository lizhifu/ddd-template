package com.ddd.infrastructure.repository;

import com.ddd.domain.car.model.vo.CarVO;
import com.ddd.domain.user.model.vo.UserCarVO;
import com.ddd.domain.user.repository.UserRepository;
import com.ddd.infrastructure.dao.CarDao;
import com.ddd.infrastructure.dao.UserDao;
import com.ddd.infrastructure.po.CarEntity;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 仓储服务-用户
 *
 * @author lizhifu
 * @date 2022/3/9
 */
@Repository
public class UserRepositoryImpl implements UserRepository {
    @Resource
    private UserDao userDao;
    @Resource
    private CarDao carDao;
    @Override
    public UserCarVO queryUserCar(Long userId) {
        List<CarEntity> carEntityList = carDao.query(userId);

        UserCarVO userCarVO = new UserCarVO();
        List<CarVO> carVOList = new ArrayList<>(carEntityList.size());
        userCarVO.setUserId(userId);
        for (CarEntity carEntity : carEntityList) {
            CarVO carVO = new CarVO();
            carVO.setCarId(carEntity.getCarId());
            carVO.setCarName(carEntity.getCarName());
            carVO.setUserId(userId);
            carVO.setCreateTime(carEntity.getCreateTime());
            carVO.setUpdateTime(carEntity.getUpdateTime());
        }
        userCarVO.setCarVOList(carVOList);
        return userCarVO;
    }
}
