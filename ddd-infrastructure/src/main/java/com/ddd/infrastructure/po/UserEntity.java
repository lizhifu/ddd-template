package com.ddd.infrastructure.po;

import lombok.Data;

import java.util.Date;

/**
 * 用户
 *
 * @author lizhifu
 * @date 2022/3/9
 */
@Data
public class UserEntity {
    /**
     * 自增ID
     */
    private Long id;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 年龄
     */
    private Integer userAge;

    /**
     * 姓名
     */
    private String userName;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;
}
