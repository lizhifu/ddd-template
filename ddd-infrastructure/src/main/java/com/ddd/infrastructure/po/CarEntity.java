package com.ddd.infrastructure.po;

import lombok.Data;

import java.util.Date;

/**
 * 汽车
 *
 * @author lizhifu
 * @date 2022/3/9
 */
@Data
public class CarEntity {
    /**
     * 自增ID
     */
    private Long id;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 车ID
     */
    private Long carId;

    /**
     * 车名称
     */
    private String carName;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;
}
