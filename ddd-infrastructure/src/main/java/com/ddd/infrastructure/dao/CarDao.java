package com.ddd.infrastructure.dao;

import com.ddd.infrastructure.po.CarEntity;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * car dao
 *
 * @author lizhifu
 * @date 2022/3/8
 */
@Mapper
public interface CarDao {
    /**
     * 插入
     * @param car
     */
    public void insert(CarEntity car);

    /**
     * 查询
     * @param userId
     * @return
     */
    public List<CarEntity> query(Long userId);
}
