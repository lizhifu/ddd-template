package com.ddd.infrastructure.dao;

import com.ddd.infrastructure.po.UserEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * user dao
 *
 * @author lizhifu
 * @date 2022/3/8
 */
@Mapper
public interface UserDao {
    /**
     * 插入
     * @param user
     */
    public void insert(UserEntity user);

    /**
     * 查询
     * @param userId
     * @return
     */
    public UserEntity query(Long userId);
}
