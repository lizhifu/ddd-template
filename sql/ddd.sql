create database ddd;

USE ddd;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
    `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
    `user_id` bigint(20) NOT NULL COMMENT '用户ID',
    `user_age` int NOT NULL COMMENT '年龄',
    `user_name` varchar(64) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '姓名',
    `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '修改时间',
    PRIMARY KEY (`id`),
    UNIQUE KEY `unique_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='用户';

DROP TABLE IF EXISTS `car`;
CREATE TABLE `car` (
 `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
 `car_id` bigint(20) NOT NULL COMMENT '车ID',
 `user_id` bigint(20) NOT NULL COMMENT '用户ID',
 `car_name` varchar(64) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '车名称',
 `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
 `update_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
 PRIMARY KEY (`id`),
 UNIQUE KEY `idx_car_id` (`car_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='奖品配置';