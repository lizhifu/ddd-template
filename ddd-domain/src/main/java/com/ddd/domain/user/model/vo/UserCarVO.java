package com.ddd.domain.user.model.vo;

import com.ddd.domain.car.model.vo.CarVO;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 用户
 *
 * @author lizhifu
 * @date 2022/3/8
 */
@Data
public class UserCarVO {
    /**
     * 用户ID
     */
    private Long userId;
    /**
     * 用户车信息
     */
    private List<CarVO> carVOList;
}
