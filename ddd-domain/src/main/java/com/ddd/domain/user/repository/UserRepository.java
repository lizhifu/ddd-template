package com.ddd.domain.user.repository;

import com.ddd.domain.user.model.vo.UserCarVO;

/**
 * 仓储服务接口-用户
 *
 * @author lizhifu
 * @date 2022/3/8
 */
public interface UserRepository {
    /**
     * 查询用户车信息
     * @param userId
     * @return
     */
    public UserCarVO queryUserCar(Long userId);
}
