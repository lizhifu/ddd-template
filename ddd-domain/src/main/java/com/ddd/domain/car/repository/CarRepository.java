package com.ddd.domain.car.repository;

import com.ddd.domain.car.model.vo.CarVO;

/**
 * 仓储服务接口-车
 *
 * @author lizhifu
 * @date 2022/3/8
 */
public interface CarRepository {
    /**
     * 增
     * @param carVO
     */
    public void addCar(CarVO carVO);

    /**
     * 查询
     * @param carId
     * @return
     */
    public CarVO queryCar(Long carId);
}
