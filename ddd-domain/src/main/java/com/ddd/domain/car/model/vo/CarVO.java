package com.ddd.domain.car.model.vo;

import lombok.Data;

import java.util.Date;

/**
 * 车
 *
 * @author lizhifu
 * @date 2022/3/8
 */
@Data
public class CarVO {
    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 车ID
     */
    private Long carId;

    /**
     * 车名称
     */
    private String carName;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间
     */
    private Date updateTime;
}
